# Games Collection

Résumé :
* `git clone https://gitlab.com/thomasperesson/games-collection.git`
* `cd games-collection`
* `composer install`
* `symfony console doctrine:database:create`
* `symfony console doctrine:migrations:migrate`
* `symfony console doctrine:schema:update --force`
* `symfony console doctrine:fixtures:load`
* `symfony serv`

**Identifiants** :
* Admin : admin@admin.fr / admin
* User : user@user / user

Pour le mot de passe oublier / email, utiliser MailHog

## Mise en place du projet
### Installation logiciel
* Installer Symfony (projet en version _5.3.10_)
* Installer Composer (dernière version)
* Installer _PHP 8.0.12_

### Récupérer le projet sur git
`git clone https://gitlab.com/thomasperesson/games-collection.git`

### Installer le projet
Quand le projet à été récupéré, aller dans le dossier du projet `cd games-collection` taper : `composer install` dans un terminal pour installer toutes les dépendances.

### Installer la BDD
S'assurer de la bonne configuration du fichier `.env`
Installer la BDD en faisant un `php bin/console doctrine:database:create` ou `symfony console doctrine:database:create`

Faire la migration des tables dans la BDD avec la commande `php bin/console doctrine:migrations:migrate` ou `symfony console doctrine:migrations:migrate`

Mettre à jour les tables : `php bin/console doctrine:schema:update --force` ou `symfony console doctrine:schema:update --force`

## Mise en place de fausses données
Charger des fausses données (fixtures) : `php bin/console doctrine:fixtures:load` ou `symfony console doctrine:fixtures:load`

## Démarrer le projet
Démarrer le serveur : `symfony serv`