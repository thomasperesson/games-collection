<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Game;
use App\Entity\Type;
use App\Entity\User;
use DateTimeImmutable;
use App\Entity\Comment;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // Création d'un utilisateur Admin
        $user = new User();
        $user->setEmail('admin@admin.fr')
            ->setNickname('admin')
            ->setRoles(['ROLE_ADMIN']);
        $password = $this->hasher->hashPassword($user, 'admin');
        $user->setPassword($password);
        $manager->persist($user);

        $user = new User();
        $user->setEmail('user@user.fr')
            ->setNickname('user')
            ->setRoles(['ROLE_USER']);
        $password = $this->hasher->hashPassword($user, 'user');
        $user->setPassword($password);
        $manager->persist($user);

        // Création de plusieurs genres
        for ($i = 1; $i <= 5; $i++) {
            $type = new Type();
            $type->setName($faker->word())
                ->setDescription($faker->paragraph())
                ->setImage($faker->image());
            $manager->persist($type);
            
            // Création de plusieurs jeux
            for ($j = 1; $j <= 2; $j++) {
                $game = new Game();
                $game->setTitle($faker->word()) 
                    ->setUrl($faker->url())
                    ->setDescription($faker->paragraph())
                    ->setCreatedAt(DateTimeImmutable::createFromMutable($faker->dateTimeBetween('-1 year', 'now')))
                    ->setImage($faker->image())
                    ->setUrl($faker->url())
                    ->setMeanRate($faker->randomFloat(1, 1, 5))
                    ->addUser($user);
                $type->addGame($game);
                $manager->persist($game);

                //Création de plusieurs commentaires
                for ($k = 1; $k <= 3; $k++) {
                    $comment = new Comment();
                    $comment->setRate($faker->numberBetween(1, 5))
                        ->setContent($faker->paragraph())
                        ->setCreatedAt(DateTimeImmutable::createFromMutable($faker->dateTimeBetween('-1 year', 'now')))
                        ->setStatus($faker->numberBetween(0, 1))
                        ->setGame($game)
                        ->setUser($user);
                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }
}
