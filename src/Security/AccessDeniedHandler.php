<?php
namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\Routing\RouterInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{

    public function __construct(RouterInterface $router, FlashBagInterface $flash)
    {
        $this->flash = $flash;
        $this->router = $router;
    }


    public function handle(Request $request, AccessDeniedException $accessDeniedException): ?Response
    {
        $this->flash->add('access_denied', 'Vous devez être connecté avec plus de privilèges pour accéder à cette page');
        return new RedirectResponse($this->router->generate('app_login'));
    }
}