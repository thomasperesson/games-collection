<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('imageFile', VichImageType::class, [
                'data_class' => null,
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'Supprimer ?',
                'download_label' => 'Télécharger l\'image',
                'asset_helper' => true,

            ])
            ->add('description', TextareaType::class)
            ->add('url', UrlType::class)
            ->add('type', EntityType::class, [
                'class'         => Type::class,
                'choice_label'  => 'name',
                'multiple'      => true,
                'expanded'      => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
