<?php

namespace App\Controller;

use App\Entity\Game;
use App\Form\GameType;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\GameRepository;
use App\Repository\CommentRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[Route('/game')]
class GameController extends AbstractController
{
    #[Route('/', name: 'game_index', methods: ['GET'])]
    public function showAllGames(GameRepository $gameRepository): Response
    {
        return $this->render('game/index.html.twig', [
            'games' => $gameRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'game_new', methods: ['GET', 'POST'])]
    #[IsGranted("ROLE_ADMIN")]
    public function createGame(Request $request): Response
    {
        $game = new Game();
        $game->setCreatedAt(new \DateTimeImmutable());
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('game/new.html.twig', compact('game', 'form'));
    }

    #[Route('/{id}', name: 'game_show', methods: ['GET', 'POST'])]
    public function showOneGame(Request $request, Game $game, CommentRepository $commentRepository): Response
    {
        $user = $this->getUser();
        $comment = new Comment();
        $gameId = $game->getId();

        $commentForm = $this->createForm(CommentType::class, $comment);
        $commentForm->handleRequest($request);
        

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {            
            $comment->setCreatedAt(new \DateTimeImmutable())
                    ->setUser($user)
                    ->setGame($game);

            // Ajout du nouveau commentaire
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            // Récupération de la moyenne des notes de tous les commentaires
            // en incluant le nouveau qui vient d'être publier
            // Puis mise à jour de la note moyenne du jeu
            $meanRate = $commentRepository->meanRate($gameId)->getContent();
            $game->setMeanRate($meanRate);
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute('game_show', ['id' => $gameId], Response::HTTP_SEE_OTHER);
        }

        return $this->render('game/show.html.twig', [
            'game' => $game,
            'commentForm' => $commentForm->createView(),
        ]);
    }

    #[Route('/add-favorite/{id}', name: 'add_favorite')]
    public function addFovorite(Game $game)
    {
        if(!$game) {
            throw new NotFoundHttpException("Aucun jeu trouvé");
        }

        $gameId = $game->getId();

        $game->addUser($this->getUser());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($game);
        $entityManager->flush();

        return $this->redirectToRoute('game_show', ['id' => $gameId], Response::HTTP_SEE_OTHER);
    }

    #[Route('/remove-favorite/{id}', name: 'remove_favorite')]
    public function removeFovorite(Game $game)
    {
        if(!$game) {
            throw new NotFoundHttpException("Aucun jeu trouvé");
        }

        $gameId = $game->getId();

        $game->removeUser($this->getUser());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($game);
        $entityManager->flush();

        return $this->redirectToRoute('game_show', ['id' => $gameId], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/edit', name: 'game_edit', methods: ['GET', 'POST'])]
    #[IsGranted("ROLE_ADMIN")]
    public function editGame(Request $request, Game $game): Response
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Le commentaire a bien été publié.');

            return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
        }
        // }

        return $this->renderForm('game/edit.html.twig', compact('game', 'form'));
    }

    #[Route('/{id}', name: 'game_delete', methods: ['POST'])]
    public function deleteGame(Request $request, Game $game): Response
    {
        if ($this->isGranted('ROLE_ADMIN') && $this->isCsrfTokenValid('delete' . $game->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($game);
            $entityManager->flush();
        }

        return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
    }
}
